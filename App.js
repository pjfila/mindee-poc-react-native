import React from 'react';

import {StackNavigator} from 'react-navigation';
import LoginScreen from "./screens/LoginScreen.js";
import DashboardScreen from "./screens/DashboardScreen.js";
import AdvisorsScreen from "./screens/AdvisorsScreen.js";
import AssistantsScreen from "./screens/AssistantsScreen.js";

const RootStack = StackNavigator({
    Login: {screen: LoginScreen},
    Dashboard: {screen: DashboardScreen},
    AdvisorsScreen: {screen: AdvisorsScreen},
    AssistantsScreen: {screen: AssistantsScreen}
    },
    {
        initialRouteName: 'Login',
    });

export default class App extends React.Component {
    render() {
        return <RootStack/>;
    }
}