import React from "react";
import {AppRegistry} from 'react-native';

// import Kitsu from 'kitsu';

// const Kitsu = require('kitsu/node')

// import { Admin, Resource } from "admin-on-rest";
// import jsonAPIRestClient from "aor-jsonapi-client/build/restClient";



export default class IO extends React.Component {

    baseUrl = 'http://fis.vyvoj.ablevyvoj.cz:5001/';

    loginDetails = {
        'username': 'test@email.cz',
        'password': 'Abc123456',
        'clientId': 'mindee-mobile-app',
        'clientSecret': 'Secret123456',
        'grantType': 'password',
        'scope': 'api-mindee offline_access'
    };

    static _instance = null;

    static getInstance() {
        if (IO._instance == null) {
            IO._instance = new IO()
        }
        return IO._instance
    }

    // this is the json-api connector object

    // restClient = null;
    access_token = null;

    login() {
        let loginFormBody = [];
        for (let property in this.loginDetails) {
            let encodedKey = encodeURIComponent(property);
            let encodedValue = encodeURIComponent(this.loginDetails[property]);
            loginFormBody.push(encodedKey + "=" + encodedValue);
        }
        loginFormBody = loginFormBody.join("&");

        return fetch(this.baseUrl + 'account/login', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded'
            },
            body: loginFormBody
        })
            .then((response) => response.json())
            .then((response) => {
                console.debug(response);
                this.access_token = response.access_token;
                // this.restClient = jsonAPIRestClient(this.baseUrl);
                // this.api = new Kitsu({
                //     baseURL: this.baseUrl + 'api-mindee/',
                //     headers: {
                //         Authorization: "Bearer "+access_token
                //     }
                // });
                // console.debug('New Kitsu API created')
            })
            .catch((error) => {
                console.error(error);
            });

    }

    get_advisors(callback) {

        return fetch(this.baseUrl + 'api-mindee/advisors', {
            method: 'GET',
            headers: {
                Authorization: 'Bearer ' + this.access_token,
                Accept: 'application/json',
                'Content-Type': 'application/vnd.api+json'
            }
        })
            .then((response) => response.json())
            .then((responseJson) => {
                callback(responseJson);
            })
            .catch((error) => {
                    console.error(error)
            });
    }

    get_assistants(callback) {

        return fetch(this.baseUrl + 'api-mindee/assistants', {
            method: 'GET',
            headers: {
                Authorization: 'Bearer ' + this.access_token,
                Accept: 'application/json',
                'Content-Type': 'application/vnd.api+json'
            }
        })
            .then((response) => response.json())
            .then((responseJson) => {
                callback(responseJson)
            })
            .catch((error) => {
                console.error(error)
            });
    }
}


AppRegistry.registerComponent('MindeePoC', () => IO);