import React from "react";
import IO from "../IO.js";
import {AppRegistry, StyleSheet, View, Text, Button, FlatList, ActivityIndicator} from "react-native";


export default class AdvisorsScreen extends React.Component {

    io = IO.getInstance()

    constructor(props){
        super(props);
        this.state ={ isLoading: true}
    }

    componentDidMount(){

        return this.io.get_advisors((responseJson) => {

                console.log(responseJson);
                this.setState({
                    isLoading: false,
                    dataSource: responseJson.data,
                }, function(){

                });

        }
        )};

    render()
    {
        if(this.state.isLoading){
            return(
                <View style={{flex: 1, padding: 20}}>
                    <ActivityIndicator/>
                </View>
            )
        }

        return (
            <View style={styles.container}>
                <FlatList
                    data={this.state.dataSource}
                    renderItem={({item}) => <Text>{item.attributes.firstName} {item.attributes.lastName}:{item.attributes.company}:{item.attributes.broker}</Text>}
                    keyExtractor={(item, index) => index}
                />
            </View>
        );
    }

}

const styles = StyleSheet.create({
    container: {
        backgroundColor: '#fff',
        alignItems: 'center',
        justifyContent: 'center',
    },
});

AppRegistry.registerComponent('MindeePoC', () => AdvisorsScreen);