import * as React from 'react';
import { StyleSheet, Dimensions, AppRegistry} from 'react-native';
import { TabViewAnimated, TabBar, SceneMap } from 'react-native-tab-view';
import AdvisorsScreen from "./AdvisorsScreen.js";
import AssistantsScreen from "./AssistantsScreen.js";
import SettingsScreen from "./SettingsScreen.js";

const initialLayout = {
    height: 0,
    width: Dimensions.get('window').width,
};

const AdvisorsRoute = () => <AdvisorsScreen style={[ styles.container, { backgroundColor: '#ff4081' } ]} />;
const AssistantsRoute = () => <AssistantsScreen style={[ styles.container, { backgroundColor: '#673ab7' } ]} />;
const SettingsRoute = () => <SettingsScreen style={[ styles.container, { backgroundColor: '#303ab7' } ]} />;

export default class DashboardScreen extends React.Component {
    state = {
        index: 0,
        routes: [
            { key: 'advisors', title: 'Advisors' },
            { key: 'assistants', title: 'Assistants' },
            { key: 'settings', title: 'Settings' },
        ],
    };

    _handleIndexChange = index => this.setState({ index });

    _renderHeader = props => <TabBar {...props} />;

    _renderScene = SceneMap({
        advisors: AdvisorsRoute,
        assistants: AssistantsRoute,
        settings: SettingsRoute
    });

    render() {
        return (
            <TabViewAnimated
                navigationState={this.state}
                renderScene={this._renderScene}
                renderHeader={this._renderHeader}
                onIndexChange={this._handleIndexChange}
                initialLayout={initialLayout}
            />
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
});

AppRegistry.registerComponent('MindeePoC', () => DashboardScreen);