import React from "react";
import IO from "../IO.js";
import {AppRegistry, StyleSheet, View, Button, Text} from "react-native";

export default class LoginScreen extends React.Component {

    io = IO.getInstance();

    render() {
        return (
            <View style={styles.container}>
                <Text>Shake your phone to open the developer menu.</Text>
                <Button
                    onPress={() => this.onPressLogin()}
                    title="LOGIN"
                    color="#841584"
                    accessibilityLabel="Learn more about this purple button"
                />
            </View>
        );
    }

    onPressLogin() {
        const { navigate } = this.props.navigation;
        console.debug('button pressed')
        this.io.login().then(() => navigate('Dashboard'))
    }

}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff',
        alignItems: 'center',
        justifyContent: 'center',
    },
});

AppRegistry.registerComponent('MindeePoC', () => LoginScreen);