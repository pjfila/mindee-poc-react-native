import React from "react";
import {AppRegistry, StyleSheet, View, Text, Switch} from "react-native";

export default class SettingsScreen extends React.Component {

    constructor() {
        super();
        this.state = {
            switch1Value: false,
            switch2Value: false
        }
    }

    render() {
        return (
            <View style={styles.container}>
                <Text>SETTINGS</Text>
                <Text>Use anonymous statistics</Text>
                <Switch onValueChange = {(value) => this.state.switch1Value = !value}
                        value = {this.state.switch1Value} />
                <Text>Cache documents offline</Text>
                <Switch onValueChange = {(value) => this.state.switch2Value = !value}
                        value = {this.state.switch2Value} />
            </View>
        );
    }

}

const styles = StyleSheet.create({
    container: {
        backgroundColor: '#fff',
        alignItems: 'center',
        justifyContent: 'center',
    },
});

AppRegistry.registerComponent('MindeePoC', () => SettingsScreen);